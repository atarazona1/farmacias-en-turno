import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DrugstoreDto } from './dto/drougstoreDto';

let counter = 0;

function getCounter() {
  return (++counter).toString().padStart(2, '0');
}

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });

    it(`Test ${getCounter()} -- Region 7`, async () => {
      const response = await appController.getDrugstoreByRegion('7');
      expect(response).toBeDefined();
      expect((response as DrugstoreDto[]).length).toBe(1721);
      (response as DrugstoreDto[]).forEach(element => {
        expect(element).toBeInstanceOf(DrugstoreDto);
      });
    });

    it(`Test ${getCounter()} -- Region 7, comuna Santiago`, async () => {
      const idRegion: string = '7';
      const filters = { neighborhood: 'Santiago' };
      const response = await appController.getDrugstoreByRegion(idRegion, filters);
      expect(response).toBeDefined();
      expect((response as DrugstoreDto[]).length).toBe(250);
      (response as DrugstoreDto[]).forEach(element => {
        expect(element).toBeInstanceOf(DrugstoreDto);
      });
    });

    it(`Test ${getCounter()} -- Region 7, local Ahumada`, async () => {
      const idRegion: string = '7';
      const filters = { local_name: 'Ahumada' };
      const response = await appController.getDrugstoreByRegion(idRegion, filters);
      expect(response).toBeDefined();
      expect((response as DrugstoreDto[]).length).toBe(185);
      (response as DrugstoreDto[]).forEach(element => {
        expect(element).toBeInstanceOf(DrugstoreDto);
      });
    });

    it(`Test ${getCounter()} -- Region 7, local Ahumada y comuna Santiago`, async () => {
      const idRegion: string = '7';
      const filters = { local_name: 'Ahumada', neighborhood: 'Santiago' };
      const response = await appController.getDrugstoreByRegion(idRegion, filters);
      expect(response).toBeDefined();
      expect((response as DrugstoreDto[]).length).toBe(16);
      (response as DrugstoreDto[]).forEach(element => {
        expect(element).toBeInstanceOf(DrugstoreDto);
      });
    });

  });
});
