/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable, HttpService } from '@nestjs/common';
import { DrugstoreDto } from './dto/drougstoreDto';
import { isNullOrUndefined } from 'util';

@Injectable()
export class AppService {

  httpService: HttpService;

  constructor() {
    this.httpService = new HttpService();
  }

  async getDrugstoreByRegion(idRegion: string, filters: any) {
    const url = `https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=${idRegion}`;

    try {
      let externalResponse = (await this.httpService.get(url).toPromise()).data;

      if (!isNullOrUndefined(filters) && !isNullOrUndefined(filters.neighborhood)) {
        externalResponse = externalResponse.filter(drugstore => drugstore.comuna_nombre.toLowerCase().includes(filters.neighborhood.toLowerCase()));
      }
      if (!isNullOrUndefined(filters) && !isNullOrUndefined(filters.local_name)) {
        externalResponse = externalResponse.filter(drugstore => drugstore.local_nombre.toLowerCase().includes(filters.local_name.toLowerCase()));
      }

      const response: DrugstoreDto[] = externalResponse.map(x => {
        const resp: DrugstoreDto = new DrugstoreDto();
        resp.comuna_nombre = x.comuna_nombre;
        resp.local_nombre = x.local_nombre;
        resp.local_direccion = x.local_direccion;
        resp.local_telefono = x.local_telefono;
        resp.local_lat = x.local_lat;
        resp.local_long = x.local_lng;
        return resp;
      });

      return response;
    } catch (error) {
      console.log('Error: ', error);
      return error;
    }
  }
  getHello(): string {
    return 'Hello World!';
  }


}
