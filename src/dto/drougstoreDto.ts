export class DrugstoreDto {
    comuna_nombre: string;
    local_nombre: string;
    local_direccion: string;
    local_telefono: string;
    local_lat: string;
    local_long: string;
}