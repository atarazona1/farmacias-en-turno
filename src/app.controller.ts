import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('region/:id')
  getDrugstoreByRegion(@Param('id') idRegion: string, @Body() filters: any = null) {
    return this.appService.getDrugstoreByRegion(idRegion, filters);
  }
}
